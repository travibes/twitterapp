package resolver

import (
	"databaseobject"
	"schema"

	"github.com/graphql-go/graphql"
)

var QueryType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			"userpostlist": &graphql.Field{
				Type:        graphql.NewList(schema.PostType),
				Description: "get post lists of a user",
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.Int,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					id, ok := p.Args["id"].(int)
					if ok {
						//Find post to corresponding ID
						//posts := []schema.Post{schema.Post{ID: 1, Title: "Chicha Morada", Content: "Chicha morada is a beverage originated in the Andean regions of Perú but is actually consumed at a national level (wiki)"}, schema.Post{ID: 2, Title: "Chicha de jora", Content: "Chicha de jora is a corn beer chicha prepared by germinating maize, extracting the malt sugars, boiling the wort, and fermenting it in large vessels (traditionally huge earthenware vats) for several days (wiki)"}}
						var posts []schema.Post
						databaseobject.DbObject.Where(&schema.Post{UserID: id}).Find(&posts)
						return posts, nil
					}
					return nil, nil
				},
			},
			"getfeed": &graphql.Field{
				Type:        graphql.NewList(graphql.NewList(schema.PostType)),
				Description: "Get posts list of all followers of that user",
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.Int,
					},
				},
				Resolve: func(params graphql.ResolveParams) (interface{}, error) {
					//posts := []schema.Post{schema.Post{ID: 1, Title: "Chicha Morada", Content: "Chicha morada is a beverage originated in the Andean regions of Perú but is actually consumed at a national level (wiki)"}, schema.Post{ID: 2, Title: "Chicha de jora", Content: "Chicha de jora is a corn beer chicha prepared by germinating maize, extracting the malt sugars, boiling the wort, and fermenting it in large vessels (traditionally huge earthenware vats) for several days (wiki)"}}
					id, ok := params.Args["id"].(int)
					var followings []schema.Follower
					if ok {
						var userFeeds [][]schema.Post
						// Get followers of a user
						databaseobject.DbObject.Where(&schema.Follower{UserID: id}).Find(&followings)
						//iterate over each Follower struct object and make a list of posts
						for _, follower := range followings {
							var posts []schema.Post
							// Find posts corresponding to each following in posts table
							databaseobject.DbObject.Where(&schema.Post{UserID: int(follower.FollowerID)}).Find(&posts)
							userFeeds = append(userFeeds, posts)
						}
						return userFeeds, nil
					}
					return nil, nil
				},
			},
			"listpost": &graphql.Field{
				Type:        graphql.NewList(schema.PostType),
				Description: "List all post of a user",
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.Int,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					//id, ok := p.Args["id"].(int64)
					//user := schema.User{ID: id, Name: "prakash", Email: "prakashveer.nith@gmail.com", Password: "fwfwfb57757jbkfkbfkw", Post: []schema.Post{schema.Post{ID: 1, Title: "Feeling Happy", Content: "I have been feeling very positive and full of energy since last 1 year. God bless me!!"}, schema.Post{ID: 2, Title: "The great democracy", Content: "Democracy is the most important pillar of any sovereign country!!"}}}
					posts := []schema.Post{schema.Post{ID: 1, Title: "Feeling Happy", Content: "I have been feeling very positive and full of energy since last 1 year. God bless me!!"}, schema.Post{ID: 2, Title: "The great democracy", Content: "Democracy is the most important pillar of any sovereign country!!"}}
					// ok {
					//find all posts corresponding to that userId
					return posts, nil
					//}
					//return nil, nil
				},
			},
			"getotherusers": &graphql.Field{
				Type:        graphql.NewList(schema.UserType),
				Description: "GET list of other users",
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.Int,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					id, ok := p.Args["id"].(int)
					if ok {
						var users []schema.User
						databaseobject.DbObject.Not([]int{id}).Find(&users)
						return users, nil
					}
					return nil, nil
				},
			},
		},
	})
