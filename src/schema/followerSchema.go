package schema

import (
	"github.com/graphql-go/graphql"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Follower struct {
	ID         int `gorm:"primary_key;AUTO_INCREMENT;not null" json:"id"`
	UserID     int `gorm:"not null" json:"userid"`
	FollowerID int `gorm:"not null" json:"followerid"`
}

var FollowerType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Follower",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.Int,
			},
			"userid": &graphql.Field{
				Type: graphql.Int,
			},
			"followerid": &graphql.Field{
				Type: graphql.Int,
			},
		},
	},
)
