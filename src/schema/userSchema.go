package schema

import (
	"github.com/graphql-go/graphql"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type User struct {
	ID             int    `gorm:"primary_key;AUTO_INCREMENT;not null" json:"id"`
	Name           string `gorm:"type:varchar(100);not null" json:"name"`
	Email          string `gorm:"type:varchar(100);not null" json:"email"`
	Password       string `gorm:"type:varchar(100);not null" json:"password"`
	FollowersCount int    `gorm:"default:0;not null" json:"followerscount"`
	FollowingCount int    `gorm:"default:0;not null" json:"followingcount"`
	PostCount      int    `gorm:"default:0;not null" json:"postcount"`
	Posts          []Post `json:"post"`
	Follower       []Follower `json:"follower"`
}

var UserType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "User",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.Int,
			},
			"name": &graphql.Field{
				Type: graphql.String,
			},
			"email": &graphql.Field{
				Type: graphql.String,
			},
			"password": &graphql.Field{
				Type: graphql.String,
			},
			"followerscount": &graphql.Field{
				Type: graphql.Int,
			},
			"followingcount": &graphql.Field{
				Type: graphql.Int,
			},
			"postcount": &graphql.Field{
				Type: graphql.Int,
			},
			"post": &graphql.Field{
				Type: graphql.NewList(PostType),
			},
			"follower": &graphql.Field{
				Type: graphql.NewList(FollowerType),
			},
		},
	},
)
