package schema

import (
	"github.com/graphql-go/graphql"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Post struct {
	ID      int    `gorm:"primary_key;AUTO_INCREMENT;not null" json:"id"`
	Title   string `gorm:"type:varchar(100);not null" json:"title"`
	Content string `gorm:"type:varchar(500);not null" json:"content"`
	UserID  int    `gorm:"not null" json:"userid"`
}

var PostType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Post",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.Int,
			},
			"title": &graphql.Field{
				Type: graphql.String,
			},
			"content": &graphql.Field{
				Type: graphql.String,
			},
			"userid": &graphql.Field{
				Type: graphql.Int,
			},
		},
	},
)
