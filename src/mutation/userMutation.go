package mutation

import (
	"fmt"

	"github.com/graphql-go/graphql"

	"databaseobject"
	"schema"
)

var MutationType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Mutation",
	Fields: graphql.Fields{
		//create new user/signup
		"signup": &graphql.Field{
			Type:        schema.UserType,
			Description: "Create new user",
			Args: graphql.FieldConfigArgument{
				//"id": &graphql.ArgumentConfig{
				//	Type: graphql.NewNonNull(graphql.Int),
				//},
				"name": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"email": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"password": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				//"post": &graphql.ArgumentConfig{
				//Type: graphql.NewList(schema.PostType),
				//	},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				fmt.Println("inside mutation resolve")
				//rand.Seed(time.Now().UnixNano())
				user := schema.User{Name: params.Args["name"].(string), Email: params.Args["email"].(string), Password: params.Args["password"].(string)}
				if databaseobject.DbObject.NewRecord(user) != false {
					databaseobject.DbObject.Create(&user)
				}
				return user, nil
			},
		},
		"login": &graphql.Field{
			Type:        schema.UserType,
			Description: "User logged in",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"email": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"password": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				//"post": &graphql.ArgumentConfig{
				//	Type: graphql.NewList(schema.PostType),
				//},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				//users := []schema.User{schema.User{ID: 1, Name: "prakash", Email: "prakashveer.nith@gmail.com", Password: "fwfwfb57757jbkfkbfkw", Post: []schema.Post{schema.Post{ID: 1, Title: "My biography", Content: "I am hero of myself"}}}, schema.User{ID: 2, Name: "raja", Email: "prakashveer15792@gmail.com", Password: "fwfwfb57757jbkfkbf34r", Post: []schema.Post{schema.Post{ID: 2, Title: "My biography", Content: "I am hero of myself"}}}}
				email, e := params.Args["email"].(string)
				password, p := params.Args["password"].(string)
				//var users []schema.User // to remove this and uncomment upper statement

				if email != "" && password != "" && e && p {
					//find user corresponding to email and password in users a
					var user schema.User
					databaseobject.DbObject.Where(&schema.User{Email: email, Password: password}).First(&user)
					return user, nil
				}
				return nil, nil
			},
		},
		"posttweet": &graphql.Field{
			Type:        schema.PostType,
			Description: "User will post a tweet",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"title": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"content": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"userid": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				//rand.Seed(time.Now().UnixNano())
				post := schema.Post{Title: params.Args["title"].(string), Content: params.Args["content"].(string), UserID: params.Args["userid"].(int)}
				//tweet := schema.Post{ID: int(rand.Intn(100000)), Title: "Feeling Happy", Content: "I have been feeling very positive and full of energy since last 1 year. God bless me!!"}
				databaseobject.DbObject.Create(&post)
				return post, nil
			},
		},
		"followuser": &graphql.Field{
			Type:        schema.FollowerType,
			Description: "Follow a user",
			Args: graphql.FieldConfigArgument{
				"userid": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"followerid": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				userid, u := params.Args["userid"].(int)
				followerid, f := params.Args["followerid"].(int)
				if u && f {
					follower := schema.Follower{UserID: userid, FollowerID: followerid}
					databaseobject.DbObject.Create(&follower)
					return follower, nil
				}
				return nil, nil
			},
		},
	},
})
