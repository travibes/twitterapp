package main

import (
	"fmt"
	"net/http"

	"databaseobject"
	"mutation"
	"resolver"
	"schema"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var Schema, _ = graphql.NewSchema(
	graphql.SchemaConfig{
		Query:    resolver.QueryType,
		Mutation: mutation.MutationType,
	},
)

func initializeGormDatabase() {
	databaseobject.DbObject, databaseobject.Error = gorm.Open("mysql", "prakash:9882550689@/twitter?charset=utf8&parseTime=True&loc=Local")
	fmt.Println("wfwf")
	if databaseobject.Error != nil {
		fmt.Println("error occured")
	}
}

func makeTables() {
	if databaseobject.DbObject.HasTable(&schema.User{}) != true {
		databaseobject.DbObject.AutoMigrate(&schema.User{})
	}
	if databaseobject.DbObject.HasTable(&schema.Post{}) != true {
		databaseobject.DbObject.AutoMigrate(&schema.Post{})
		databaseobject.DbObject.Model(&schema.Post{}).AddForeignKey("user_id", "users(id)", "CASCADE", "CASCADE")
	}
	if databaseobject.DbObject.HasTable(&schema.Follower{}) != true {
		databaseobject.DbObject.AutoMigrate(&schema.Follower{})
		databaseobject.DbObject.Model(&schema.Follower{}).AddForeignKey("user_id", "users(id)", "CASCADE", "CASCADE")
		databaseobject.DbObject.Model(&schema.Follower{}).AddForeignKey("follower_id", "users(id)", "CASCADE", "CASCADE")
	}
}

func main() {

	//initializing database object
	initializeGormDatabase()

	// Create tables
	makeTables()
	//db.Model(&schema.User{}).Related(&schema.Post{})
	//db.CreateTable(&schema.User{})
	fmt.Println("after user")
	//.CreateTable(&schema.Post{})
	fmt.Println("after post")
	//db.CreateTable(&schema.Follower{})
	fmt.Println("tables created")

	h := handler.New(&handler.Config{
		Schema:   &Schema,
		Pretty:   true,
		GraphiQL: true,
	})

	http.Handle("/graphql", h)
	http.ListenAndServe(":8080", nil)
	fmt.Println("started server")
}
